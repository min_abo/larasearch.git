<?php
return[
    'elasticsearch' => [
        'hosts' => [ '127.0.0.1:9200' ],
        'index' => '_index',
        'type' => '_type',

        // 是否开启记录日志
        'open_log' => false,
        'log_path' => storage_path('logs/elasticsearch_'.date( 'Y-m-d' ).'.log'),

        /**
         * 连接池 ( Connection Pool )
         *
         * 可选项
         * Elasticsearch\ConnectionPool\StaticNoPingConnectionPool::class
         * Elasticsearch\ConnectionPool\SimpleConnectionPool::class
         * Elasticsearch\ConnectionPool\SniffingConnectionPool::class
         * Elasticsearch\ConnectionPool\StaticConnectionPool::class
         */
        'connection_pool' => Elasticsearch\ConnectionPool\StaticNoPingConnectionPool::class,

        /**
         * 连接查询器 Connection Selector
         *
         * 可选项:
         * Elasticsearch\ConnectionPool\Selectors\StickyRoundRobinSelector::class
         * Elasticsearch\ConnectionPool\Selectors\RoundRobinSelector::class
         * Elasticsearch\ConnectionPool\Selectors\RandomSelector::class
         */
        'selector' => Elasticsearch\ConnectionPool\Selectors\StickyRoundRobinSelector::class,
    ],
    'redisbitmap' => [
        'client' => 'phpredis',
        'host' => env( 'REDIS_HOST', '127.0.0.1:6379' ),
        'password' => env( 'REDIS_PASSWORD', null ),
        'port' => env( 'REDIS_PORT', 6379 ),
        'database' => 0,
        'parameters'=>[
            'password' => env('REDIS_PASSWORD', null)
        ],

        'index' => '[rbm]:',
    ],
];