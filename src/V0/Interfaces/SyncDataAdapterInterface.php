<?php
namespace Abo\Larasearch\V0\Interfaces;

/**
 * Interface SyncChangeDataInterface
 * Description: 变更数据 同步接口
 * @package Abo\Larasearch\Interfaces
 */
Interface SyncDataAdapterInterface
{
    public function syncInsertData( array $insertData, $addParam = '' );

    public function syncUpdateData( array $updateData, $addParam = '' );

    public function syncDeleteData( array $deleteData, $addParam = '' );
}