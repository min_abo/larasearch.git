<?php
namespace Abo\Larasearch\V0\Interfaces;

/**
 * Interface SyncChangeDataInterface
 * Description: 变更数据 同步接口
 * @package Abo\Larasearch\Interfaces
 */
Interface SyncChangeDataInterface
{
    /** 数据初始 方法 */
    public function initData2Index();

    /** 主更新方法 */
    public function syncData2Index();

    /** 格式化新增数据 */
    public function formatChangeData2Sync( string $changeType );
}