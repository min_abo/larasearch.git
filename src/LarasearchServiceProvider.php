<?php

namespace Abo\Larasearch;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Abo\Larasearch\V0\ElasticSearch\Builder;
use Abo\Larasearch\V0\ElasticSearch\Grammar;
use Abo\Larasearch\V0\ElasticSearch\ElasticSearchProvider;

/**
 * Class LarasearchServiceProvider
 * @package Abo\Larasearch
 */
class LarasearchServiceProvider extends ServiceProvider
{
    protected $commands = [
        'Abo\Larasearch\Console\MakeElasticSearchSyncCommand',
    ];

    public function boot()
    {
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'Config' => config_path(),
        ]);
    }

    public function register()
    {
        $this->commands( $this->commands );

        // 发布配置文件
        $this->mergeConfig();

        // 绑定 ES链接实例
        $ElasticSearchProvider = new ElasticSearchProvider( $this->app );
        $ElasticSearchProvider->bindBuilder();
    }


    /** 发布配置文件 @return void */
    protected function mergeConfig()
    {
        $configFile = __DIR__ . DIRECTORY_SEPARATOR . 'V0/Config/larasearch.php';
        if (file_exists($configFile)) {
            $this->mergeConfigFrom($configFile, 'larasearch');
        }
    }
}